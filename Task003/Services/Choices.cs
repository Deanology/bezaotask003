﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task003.Services
{
    /*
        * <Summary>
        * This class implements WHILE statement, SWITCH statement, BREAK statement and the IF statement
        * </Summary>
        */
    class Choices
    {
        static List<string> squad = new List<string>();
        
        //<summary>Display of Choices </summary>
        static void DisplayMenu()
        {
            
            Console.WriteLine("\n Menu");
            Console.WriteLine("===========================\n");
            Console.WriteLine(" A - Add Player(s) to the Squad");
            Console.WriteLine(" B - Show Player(s) in the squad");
            Console.WriteLine(" C - Check Available Players (Injury Free)");
            Console.WriteLine(" D - Convert Players names to Uppercase");
            Console.WriteLine(" E - Check out all switch methods we have");
            Console.WriteLine(" F - Check out switch when");
            Console.WriteLine(" Q - Quit\n");
            Console.WriteLine("===========================\n");
        }

        static int getMenuChoice()
        {
            int option = 0;
            bool cont = true;
            string buffer;
            while (cont == true)
            {
                DisplayMenu();
                Console.WriteLine("==========Choose a Option==========");
                buffer = Console.ReadLine();

                switch (buffer)
                {
                    case "a":
                    case "A":
                        option = 1;
                        cont = false;
                        break;

                    case "b":
                    case "B":
                        option = 2;
                        cont = false;
                        break;

                    case "c":
                    case "C":
                        option = 3;
                        cont = false;
                        break;

                    case "d":
                    case "D":
                        option = 4;
                        cont = false;
                        break;

                    case "e":
                    case "E":
                        option = 5;
                        cont = false;
                        break;

                    case "f":
                    case "F":
                        option = 6;
                        cont = false;
                        break;
                    case "q":
                    case "Q":
                        option = 0;
                        cont = false;
                        break;
                    default:
                        Console.WriteLine($"\n\n-- > {buffer} is not valid < --\n\n");
                        break;
                }
            }
            return option;
        }
        public static void option()
        {
            int maxNumberOfPlayers = 99;
            do
            {
                maxNumberOfPlayers = getMenuChoice();
                switch (maxNumberOfPlayers)
                {
                    case 0:
                        break;
                    case 1:
                        ImplementingForEach.addPlayer(squad);
                        break;
                    case 2:
                        ImplementingForEach.showPlayers(squad);
                        break;
                    case 3:
                        ImplementingForEach.playersAvailability(squad);
                        break;
                    case 4:
                        ForLoop.convertToUpperCase(squad);
                        break;
                    case 5:
                        Switch.switchOption();
                        break;
                    case 6:
                        runSwitchWhen.SwitchWhen();
                        break;
                    default:
                        Console.WriteLine("\n\nError... Invalid menu option.");
                        break;
                }
                if (maxNumberOfPlayers != 0)
                {
                    Console.Write("\nPress < ENTER > to continue...");
                    Console.ReadLine();
                }
            } while (maxNumberOfPlayers != 0);
        }

    }
}
