﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task003.Services
{
 
        
    public abstract class Shape
    {
        public abstract double Area { get; }
        public abstract double Circumference { get; }
    }
    public class Rectangle : Shape
    {

        public Rectangle(double length, double width)
        {
            this.Length = length;
            this.Width = width;
        }

        public double Length { get; set; }
        public double Width { get; set; }
        public override double Area
        {
            get { return Math.Round(Length * Width, 2); }
        }
        public override double Circumference
        {
            get { return (Length + Width) * 2; }
        }
    }
    public class Square : Rectangle
    {
        public double Side
        {
            get; set;
        }
        public Square(double side) : base(side, side)
        {
            this.Side = side;
        }
    }
    public class Circle : Shape
    {
        public double Radius { get; set; }
        public Circle(double radius)
        {
            this.Radius = radius;
        }
        public override double Area
        {
            get { return Math.PI * Math.Pow(Radius, 2); }
        }
        public override double Circumference
        {
            get { return 2 * Math.PI * Radius; }
        }
    }
}

