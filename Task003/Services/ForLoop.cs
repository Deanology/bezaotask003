﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task003.Services
{
    /*
     * <Summary>
     * This class implements FORLOOP statement
     * </Summary>
     */
    class ForLoop
    {
        public static void convertToUpperCase(List<string> squad)
        {
            for (int i = 0; i <squad.Count-1; i++)
            {
                Console.WriteLine(squad[i].ToUpper());
            }
        }
    }
}
