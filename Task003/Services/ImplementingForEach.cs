﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task003.Services
{
    /*
     * <Summary>
     * This class implements FOREACH statement, CONTINUE statement, IF statement and ELSE statement
     * </Summary>
     */
    class ImplementingForEach
    {
        
        public static void addPlayer(List<string> squad)
        {
            Console.WriteLine("Enter player name to add to the list");
            string player = Console.ReadLine();
                squad.Add(player);                       
        }
       public static void showPlayers(List<string> squad)
        {
            int number = 1;
            Console.WriteLine("Chelsea Football Club Squad for 2019/2020 Season");
            if (squad.Count == 0)
            {
                Console.WriteLine("Empty Squad");
            }
            else
            {
                foreach (var player in squad)
                {
                    Console.WriteLine($"{number}. \t {player}");
                    number++;
                }
            }
            
        }

        public static void playersAvailability(List<string> squad)
        {
            Console.WriteLine("Players available for today's game are:");
            foreach (var player in squad)
            {
                try
                {
                    if (player.StartsWith("I"))
                    {
                        continue;                        
                    }
                    Console.WriteLine($"{player}");
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
                
            }
        }
    }
}
