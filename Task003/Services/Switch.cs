﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;

namespace Task003.Services
{
    public enum Color
    {
        Red, Green, Blue
    }
    class Switch
    {
        public static void switch1()
        {
            int caseSwitch = 1;
            switch (caseSwitch)
            {
                case 1:
                    Console.WriteLine("Case 1");
                    break;
                case 2:
                    Console.WriteLine("Case 2");
                    break;
                default:
                    Console.WriteLine("Case 2");
                    break;
            }
        }
        public static void switch2()
        {
            Color color = (Color)(new Random()).Next(0, 3);
            switch (color)
            {
                case Color.Red:
                    Console.WriteLine("The color is red");
                    break;
                case Color.Green:
                    Console.WriteLine("The color is green");
                    break;
                case Color.Blue:
                    Console.WriteLine("The color is blue");
                    break;
                default:
                    Console.WriteLine("The color is unknown");
                    break;
            }
        }
        public static void switch3()
        {
            Random random = new Random();
            int caseSwitch = random.Next(1, 4);

            switch (caseSwitch)
            {
                case 1:
                    Console.WriteLine("Case 1");
                    break;
                case 2:
                case 3:
                    Console.WriteLine("Case 2");
                    break;
                default:              
                    Console.WriteLine($"An Unexpected value {caseSwitch}");
                    break;

            }
        }
        public static void switch4()
        {
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                case DayOfWeek.Saturday:
                    Console.WriteLine("The Weekend");
                    break;
                case DayOfWeek.Monday:
                    Console.WriteLine("The firstday of work week");
                    break;
                case DayOfWeek.Friday:
                    Console.WriteLine("The lastday of work week");
                    break;
                default:
                    Console.WriteLine("The middle of work week");
                    break;
            }
        }
        public static void switch5()
        {
            Console.WriteLine("Coffee sizes: 1 = small 2 = medium 3: large");
            Console.WriteLine("Please enter your selection: ");
            String value = Console.ReadLine();
            /*int value = Int32.Parse(str);*/
            int cost = 0;

            switch (value)
            {
                case "1":
                case "small":
                    cost += 25;
                    break;
                case "2":
                case "medium":
                    cost += 25;
                    goto case "1";
                case "3":
                case "large":
                    cost += 50;
                    goto case "1";
                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2 or 3");
                    break;
            }
            if (cost != 0)
            {
                Console.WriteLine($"Please insert {cost} cents");
            }
            Console.WriteLine("Thanks for your patronage");
        }
        public static void switch6()
        {
            int[] values = { 2, 4, 6, 8, 10 };
            showCollectionInformation(values);

            var list = new List<string>();
            list.AddRange(new string[] {"Adam", "Abigail", "Betrand", "Bridgette" });
            showCollectionInformation(list);

            List<int> numbers = null;
            showCollectionInformation(numbers);
        }
        public static void showCollectionInformation(object coll)
        {
            switch (coll)
            {
                case Array arr:
                    Console.WriteLine($"An array with {arr.Length} elements.");
                    break;
                case IEnumerable<int> ieInt:
                    Console.WriteLine($"Average: {ieInt.Average(s => s)}");
                    break;
                case IList list:
                    Console.WriteLine($"{list.Count} items");
                    break;
                case IEnumerable ie:
                    string result = "";
                    foreach (var item in ie)
                        result += $"{item}";
                    Console.WriteLine(result);
                    break;
                default:
                    Console.WriteLine($"A instance of type {coll.GetType().Name}");
                    break;
            }
        }
        public static void switch7()
        {
            int[] values = { 2, 4, 6, 8, 10 };
            showCollectionInformation(values);

            var list = new List<string>();
            list.AddRange(new string[] { "Adam", "Abigail", "Betrand", "Bridgette" });
            showCollectionInformation(list);

            List<int> numbers = null;
            showCollectionInformation(numbers);
        }
        public static void showCollectionInformation<T>(T coll)
        {
            switch (coll)
            {
                case Array arr:
                    Console.WriteLine($"An array with {arr.Length} elements.");
                    break;
                case IEnumerable<int> ieInt:
                    Console.WriteLine($"Average: {ieInt.Average(s => s)}");
                    break;
                case IList list:
                    Console.WriteLine($"{list.Count} items");
                    break;
                case IEnumerable ie:
                    string result = "";
                    foreach (var item in ie)
                        result += $"{item}";
                    Console.WriteLine(result);
                    break;
                case object o:
                    Console.WriteLine($"An instance of type {o.GetType().Name}");
                    break;
                default:
                    Console.WriteLine($"Null passed to this method");
                    break;
            }
        }
        public static void switchOption()
        {
            Console.WriteLine("\n Select Menu");
            Console.WriteLine("1. Switch Example 1 \n2. Switch Example 2 \n3. Switch Example 3 \n4. Switch Example 4 \n" +
                "5. Switch Example 5 \n" +
                "6. Switch Example 6 \n7. Switch Example 7 \n8. Switch Example 8 \n");
            Console.WriteLine("===========================\n");
            string buffer = Console.ReadLine();
            int value = Int32.Parse(buffer);

            switch (value)
            {
                case 1:
                    switch1();
                    break;
                case 2:
                    switch2();
                    break;
                case 3:
                    switch3();
                    break;
                case 4:
                    switch4();
                    break;
                case 5:
                    switch5();
                    break;
                case 6:
                    switch6();
                    break;
                case 7:
                    switch7();
                    break;

                default:
                    break;
            }

        }
    }
}
