﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task003.Services
{
    class runSwitchWhen
    {
        public static void SwitchWhen()
        {
            Shape shape = null;
            Shape[] shapes = { new Square(10), new Rectangle(5, 7), shape, new Square(0), new Rectangle(8, 8), new Circle(3) };
            foreach (var sh in shapes)
                showShapeInfo(sh);
        }
        public static void showShapeInfo(Shape sh)
        {
            switch (sh)
            {
                //Note that this code never evaluates to true
                case Shape shape when shape == null:
                    Console.WriteLine($"An uninitialized shape (shape == null)");
                    break;
                case null:
                    Console.WriteLine($"An uninitialized shape");
                    break;
                case Shape shape when sh.Area == 0:
                    Console.WriteLine($"The shape: {sh.GetType().Name} with no dimensioons");
                    break;
                case Square sq when sh.Area > 0:
                    Console.WriteLine("Information about square:");
                    Console.WriteLine($"    Length of a side: {sq.Side}");
                    Console.WriteLine($"    Area:  {sq.Area}");
                    break;
                case Rectangle rec when rec.Length == rec.Width && rec.Area > 0:
                    Console.WriteLine("Information about rectangle:");
                    Console.WriteLine($"    Length of a side: {rec.Length}");
                    Console.WriteLine($"    Area:  {rec.Area}");
                    break;
                case Rectangle rec when  rec.Area > 0:
                    Console.WriteLine("Information about rectangle:");
                    Console.WriteLine($"    Dimensions: {rec.Length} x {rec.Width}");
                    Console.WriteLine($"    Area:  {rec.Area}");
                    break;
                case Shape shape when sh != null:
                    Console.WriteLine($"A {sh.GetType().Name} shape");
                    break;
                default:
                    Console.WriteLine($"The {nameof(sh)} variable does not represent a Shape.");
                    break;
            }
        }
    }
}
